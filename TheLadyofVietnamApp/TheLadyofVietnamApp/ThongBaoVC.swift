//
//  ThongBaoVC.swift
//  TheLadyofVietnamApp
//
//  Created by Tuan Phan on 5/5/18.
//  Copyright © 2018 Roger Brothers. All rights reserved.
//

import UIKit

class ThongBaoVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var thongbaoList = ["Thông Báo Chung","Thông Báo Theo Hội","Thông Báo Theo Giáo Họ","Ý Lễ Trong Tuần"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ThongBaoCell", for: indexPath) as? ThongBaoCell {
            cell.title.text = thongbaoList[indexPath.row]
            return cell
        } else {
             return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let thongbao = thongbaoList[indexPath.row]
        performSegue(withIdentifier: "ThongBaoChungVC", sender: thongbao)
        
//        switch (indexPath.row){
//        case 0, 1, 2:
//            self.performSegue(withIdentifier: "ThongBaoChungVC", sender: thongbao)
//        case 1:
//            self.performSegue(withIdentifier: "ThongBaoTheoHoiVC", sender: self)
//        case 2:
//            self.performSegue(withIdentifier: "ThongBaoTheoGiaoHoVC", sender: self)
//        case 3:
//            self.performSegue(withIdentifier: "YLeTheoTuanVC", sender: self)
//        default:
//            break;
//        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? ThongBaoChungVC {
            
            if let thongbao = sender as? String {
                destination.tb = thongbao
            }
        }
    }
 

}
