//
//  CatholicMusic.swift
//  TheLadyofVietnamApp
//
//  Created by Tuan Phan on 5/4/18.
//  Copyright © 2018 Roger Brothers. All rights reserved.
//

import Foundation

class CatholicMusic {
    private var _imageURL: String!
    private var _videoURL: String!
    private var _videoTitle: String!
    
    var imageURL: String {
        return _imageURL
    }
    
    var videoURL: String {
        return _videoURL
    }
    
    var videoTitle: String {
        return _videoTitle
    }
    
    init(imageURL: String, videoURL: String, videoTitle: String) {
        _imageURL = imageURL
        _videoURL = videoURL
        _videoTitle = videoTitle
        
    }
}
