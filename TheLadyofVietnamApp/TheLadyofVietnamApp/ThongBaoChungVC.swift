//
//  ThongBaoChungVC.swift
//  La Vang
//
//  Created by Tuan Phan on 5/7/18.
//  Copyright © 2018 Roger Brothers. All rights reserved.
//

import UIKit

class ThongBaoChungVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topLabel: UILabel!
    var thongbaoList = [ThongBao]()
    var tb: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var t0 = ThongBao(title: "", content: "")
        var t1 = ThongBao(title: "", content: "")
        var t2 = ThongBao(title: "", content: "")
        var t3 = ThongBao(title: "", content: "")
        
        if tb == "Thông Báo Chung" {
            topLabel.text = tb
            t0 = ThongBao(title: "ThongBaoChung", content: "Tuan")
            t1 = ThongBao(title: "ThongBaoChung", content: "MInh")
            t2 = ThongBao(title: "ThongBaoChung", content: "Phan")
            t3 = ThongBao(title: "ThongBaoChung", content: "TuanPhan")
        }
        else if tb == "Thông Báo Theo Hội" {
            topLabel.text = tb
            t0 = ThongBao(title: "ThongBaoTheoHoi", content: "Tuan")
            t1 = ThongBao(title: "ThongBaoTheoHoi", content: "MInh")
            t2 = ThongBao(title: "ThongBaoTheoHoi", content: "Phan")
            t3 = ThongBao(title: "ThongBaoTheoHoi", content: "TuanPhan")
        }
        else if tb == "Thông Báo Theo Giáo Họ" {
            topLabel.text = tb
            t0 = ThongBao(title: "ThongBaoTheoGiaoHo", content: "Tuan")
            t1 = ThongBao(title: "ThongBaoTheoGiaoHo", content: "MInh")
            t2 = ThongBao(title: "ThongBaoTheoGiaoHo", content: "Phan")
            t3 = ThongBao(title: "ThongBaoTheoGiaoHo", content: "TuanPhan")
        }
        else {
            topLabel.text = tb
            t0 = ThongBao(title: "YLeTheoTuan", content: "Tuan")
            t1 = ThongBao(title: "YLeTheoTuan", content: "MInh")
            t2 = ThongBao(title: "YLeTheoTuan", content: "Phan")
            t3 = ThongBao(title: "YLeTheoTuan", content: "TuanPhan")
        }
        
        thongbaoList.append(t0)
        thongbaoList.append(t1)
        thongbaoList.append(t2)
        thongbaoList.append(t3)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "TBChungCell", for: indexPath) as? TBChungCell {
            
            let thongBao = thongbaoList[indexPath.row]
            cell.updateTBChung(thongbao: thongBao)
            return cell
            
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return thongbaoList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let thongbao = thongbaoList[indexPath.row]
        performSegue(withIdentifier: "TBContentVCSegue", sender: thongbao)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            
            if let destination = segue.destination as? TBContentVC {
                
                if let tb = sender as? ThongBao {
                    destination.thongbao = tb
                }
            }
    }
}
