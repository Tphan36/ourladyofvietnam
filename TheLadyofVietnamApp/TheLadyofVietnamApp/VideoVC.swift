//
//  VideoVC.swift
//  TheLadyofVietnamApp
//
//  Created by Tuan Phan on 5/4/18.
//  Copyright © 2018 Roger Brothers. All rights reserved.
//

import UIKit

class VideoVC: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var titleLbl: UILabel!
    
    
    
    private var _song: CatholicMusic!
    
    var song: CatholicMusic {
        get {
            return _song
        }
        set {
            _song = newValue
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        titleLbl.text = song.videoTitle
        webView.loadHTMLString(song.videoURL, baseURL: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
