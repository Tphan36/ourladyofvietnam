//
//  tentVCC.swift
//  La Vang
//
//  Created by Tuan Phan on 5/8/18.
//  Copyright © 2018 Roger Brothers. All rights reserved.
//

import UIKit

class TBContentVC: UIViewController {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentView: UITextView!
    
    var thongbao: ThongBao!

    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = thongbao.title
        contentView.text = thongbao.content
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
