//
//  ViewController.swift
//  TheLadyofVietnamApp
//
//  Created by Tuan Phan on 5/4/18.
//  Copyright © 2018 Roger Brothers. All rights reserved.
//

import UIKit

class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var songs = [CatholicMusic]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let s1 = CatholicMusic(imageURL: "https://www.preachit.org/images/ppt_thumbs/1663-slide1-600.jpg", videoURL: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/JPsgIhlYQmM\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>", videoTitle: "Mary Did You Know")
        
        let s2 = CatholicMusic(imageURL: "https://giaoxusonghinh.org/uploads/news/2017_04/thanh-gia-phuc-sinh.jpg", videoURL: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/1oSKUygilQI\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>", videoTitle: "Niềm Tin Phục Sinh")
        
        let s3 = CatholicMusic(imageURL: "http://tinlanh.org/wp/wp-content/uploads/2018/03/MuaXuanPhucSinh-e1522220239955-720x340.jpg", videoURL: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/GeVWdxSaE_Q\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>", videoTitle: "Chúa Đang Phục Sinh")
        
        let s4 = CatholicMusic(imageURL: "http://longchuathuongxot.vn/v2/wp-content/uploads/2017/04/h5_resize-2-277x165.jpg", videoURL: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/PYCozoGTnKY\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>", videoTitle: "Giê-su Đấng Phục Sinh")
        
        let s5 = CatholicMusic(imageURL: "http://www.berlinertageszeitung.de/images/Ostern-2018.JPEG", videoURL: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/2d5a1Fl85Sc\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>", videoTitle: "Mừng Chúa Phục Sinh")
        
        songs.append(s1)
        songs.append(s2)
        songs.append(s3)
        songs.append(s4)
        songs.append(s5)
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell", for: indexPath) as? VideoCell {
            
            let song = songs[indexPath.row]
            cell.updateUI(catholicMusic: song)
            return cell
            
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return songs.count
    }
    // event tap
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let song = songs[indexPath.row]
        performSegue(withIdentifier: "VideoVC", sender: song)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? VideoVC {
            if let s = sender as? CatholicMusic {
                destination.song = s
            }
        }
    }
    
}

