//
//  ThongBao.swift
//  La Vang
//
//  Created by Tuan Phan on 5/5/18.
//  Copyright © 2018 Roger Brothers. All rights reserved.
//

import Foundation

class ThongBao {
    private var _title: String!
    private var _content: String!
    
    var title: String {
        return _title
    }
    
    var content: String {
        return _content
    }
    
    init(title: String, content: String) {
        _title = title
        _content = content
    }
}
