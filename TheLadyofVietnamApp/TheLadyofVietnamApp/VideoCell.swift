//
//  VideoCell.swift
//  TheLadyofVietnamApp
//
//  Created by Tuan Phan on 5/4/18.
//  Copyright © 2018 Roger Brothers. All rights reserved.
//

import UIKit

class VideoCell: UITableViewCell {

    @IBOutlet weak var videoPreviewImage: UIImageView!
    
    @IBOutlet weak var videoTitle: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // create a function so that every time a new cell is created, it will be call
    func updateUI(catholicMusic: CatholicMusic) {
        videoTitle.text = catholicMusic.videoTitle
        let url = URL(string: catholicMusic.imageURL)!
        
        //using this helping downloading image in the background thread so it will not freeze the app
        DispatchQueue.global().async {
            do {
                let data = try Data(contentsOf: url)
                DispatchQueue.global().sync {
                    self.videoPreviewImage.image = UIImage(data: data)
                }
            } catch {
                //handle the error
            }
        }
    }

}
