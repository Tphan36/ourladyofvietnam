//
//  TBChungCell.swift
//  La Vang
//
//  Created by Tuan Phan on 5/8/18.
//  Copyright © 2018 Roger Brothers. All rights reserved.
//

import UIKit

class TBChungCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateTBChung(thongbao: ThongBao) {
        titleLabel.text = thongbao.title
    }

}
