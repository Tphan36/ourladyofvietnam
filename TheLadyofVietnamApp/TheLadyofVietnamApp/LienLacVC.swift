//
//  LienLacVC.swift
//  TheLadyofVietnamApp
//
//  Created by Tuan Phan on 5/5/18.
//  Copyright © 2018 Roger Brothers. All rights reserved.
//

import UIKit

class LienLacVC: UIViewController {
    

    @IBOutlet weak var vanphongbtn: UIButton!
    @IBOutlet weak var hoidoanbtn: UIButton!
    @IBOutlet weak var hoidongbtn: UIButton!
    @IBOutlet weak var lienlacView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.vanphong()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func hoidongButton(_ sender: Any) {
        hoidongbtn.titleLabel?.textColor = UIColor.blue
        hoidoanbtn.titleLabel?.textColor = UIColor.black
        vanphongbtn.titleLabel?.textColor = UIColor.black
        lienlacView.text =
         "Chủ Tịch:\n" +
         "        Vincentê Đinh Đức Hạnh                   404-797-7792\n" +
         "\nTổng Thư Ký: \n" +
         "        Phêrô Cao Thế Hùng                          404-759-0929\n" +
        "\nKhối Hành Chánh:\n" +
        "        Giuse Mai Hoàng Minh                       678-463-9548\n" +
        "\nKhối Giáo Dục Ngành Trẻ: \n" +
        "        T.S. Phêrô Huỳnh Việt Hùng              770-368-1810\n" +
        "\nKhối Giáo Dục Tráng Niên:\n" +
        "        T.S. Giuse Nguyễn Hoà Phú              404-346-3452\n" +
        "\nKhối Phượng Tự: \n" +
        "         Bartôlômêô Trần Văn Kiệm              404-409-2536\n" +
        "\nKhối Truyền Giáo:\n" +
        "        Giuse Phan Thanh Hoàng                 770-605-9679\n" +
        "\nKhối Sinh Hoạt Cộng Đồng: \n" +
        "        Phêrô Bùi Chiến Thắng                      678-463-4947\n"
        
    }
    
    @IBAction func hoidoanButton(_ sender: Any) {
        hoidongbtn.titleLabel?.textColor = UIColor.black
        hoidoanbtn.titleLabel?.textColor = UIColor.blue
        vanphongbtn.titleLabel?.textColor = UIColor.black
        lienlacView.text =
            "Giáo Lý Trẻ Em:\n" +
            "        Thầy Phêrô Huỳnh Việt Hùng                        770-368-1810\n" +
            "\nViệt Ngữ Trẻ Em: \n" +
            "        Giuse Trần Thiên Dưỡng                               404-936-3042\n" +
            "\nGiáo Lý Dự Tòng:\n" +
            "        Phaolô Trần Đức Hậu                                    770-960-6222\n" +
            "\nDự Bị Hôn Nhân Công Giáo: \n" +
            "        Antôn Hoàng Duy                                          678-656-1896\n" +
            "\nThiếu Nhi Thánh Thể:\n" +
            "        Giuse Phạm Quang Thông Darren               404-924-5261\n" +
            "\nThừa Tác Viên Thánh Thể:\n" +
            "        Đôminicô Ngô Bá Thừa                                 678-462-9712\n" +
            "\nThừa Tác Viên Lời Chúa: \n" +
            "        Giuse Đặng Đình Hiếu                                  404-384-6785\n" +
            "\nThừa Tác Viên Hướng Dẫn: \n" +
        "        Gioan Kim Lê Văn Ngời                                 770-451-3899\n"
    }
    @IBAction func vanphongButton(_ sender: Any) {
        self.vanphong()
    }
    func vanphong() {
        hoidongbtn.titleLabel?.textColor = UIColor.black
        hoidoanbtn.titleLabel?.textColor = UIColor.black
        vanphongbtn.titleLabel?.textColor = UIColor.blue
        lienlacView.text =
        "\nVăn Phòng (Office): 770-472-9963  Nhà Xứ (Rectory): 770-471-8453  Điện Thư (Email): info@gxdmvn.org  Trang Mạng (Website): http://gxdmvn.org"
    }
}
